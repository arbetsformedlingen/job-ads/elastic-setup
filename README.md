# Elastic-setup

This repo setup elastic-search for:

* jobsearch-ads
* joblinks
* api-key-generator (writer)

The following indices are impacted:

* `platsannons*`
* `joblinks*`
* `historical*`

Currently index template and ILM are created.

## Run

Set the following environment variables adapted for your environment.

```shell
export ES_USER=my-user
export ES_PWD=secret-password
export ES_HOST=my.elastic.company.com
export ES_PORT=9243
```

Ensure you are standing in the root directory of this repository.
Then run the command `scripts/apply.sh`.

## Remove objects

If you want to remove objects such as roles, users etc. Delete the files from here and
do also delete the corresponding information in Elastic. Reasons is that there are
data we do not manage via this script and we do not want to destroy it.
