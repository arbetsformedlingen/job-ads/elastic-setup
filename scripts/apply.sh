#!/bin/bash

set -e

ES_URL=https://${ES_USER}:${ES_PWD}@${ES_HOST}:${ES_PORT}

# Add or update an index template and corresponding lifecycle.
function add_index_template {
  local NAME=$1
  [ -f config/index-templates/${NAME}.json ] || (echo ERROR: cannot create template ${NAME} && exit 1)
  [ -f config/index-lifecycles/${NAME}-ilm.json ] || (echo ERROR: cannot create ilm ${NAME} && exit 1)

  echo "Setting up index template $NAME"
  curl -s -H "Content-Type: application/json" -X PUT -d @config/index-templates/${NAME}.json ${ES_URL}/_index_template/${NAME} | jq .

  echo "Setting up ILM for index template $NAME"
  curl -s -H "Content-Type: application/json" -X PUT -d @config/index-lifecycles/${NAME}-ilm.json ${ES_URL}/_ilm/policy/${NAME}-ilm | jq .
}

# Add or update a user role
function add_user_role {
  local NAME=$1
  [ -f config/user-roles/${NAME}.json ] || (echo ERROR: cannot create role ${NAME} && exit 1)

  echo Add user role $NAME
  curl -s -H 'Content-Type: application/json' -X PUT -d @config/user-roles/$NAME.json ${ES_URL}/_security/role/$NAME | jq .

}

# Add or update a user
# If user is added, a random password is created and stored.
function add_user {
    local NAME=$1
    [ -f config/users/${NAME}.json ] || (echo ERROR: cannot create user ${NAME} && exit 1)

    if curl -s ${ES_URL}/_security/user/${NAME} | jq -e --arg name $NAME -e '.[$name]' > /dev/null ; then
        echo Updating user $NAME
        curl -s -H "Content-Type: application/json" -X PUT -d @config/users/${NAME}.json ${ES_URL}/_security/user/${NAME} | jq .
    else
        CREDENTIAL_FILE=credential-$(date +%Y%m%d)-${NAME}.txt
        echo No user $NAME, setting password stored in ${CREDENTIAL_FILE}
        PASSWORD=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 18)
        echo $PASSWORD > ${CREDENTIAL_FILE}
        cat config/users/${NAME}.json | jq --arg pwd ${PASSWORD} '. + {password: $pwd}' |\
          curl -s -H "Content-Type: application/json" -X PUT -d @- ${ES_URL}/_security/user/${NAME}
    fi
}

# Ensure necessary commands exist
(command -v curl >/dev/null && command -v jq >/dev/null)  || (echo "ERROR: Command curl or jq is missing." && exit 1)

for i in config/index-templates/*.json
do
    j=$(echo $i | sed 's/.*\/\([^.]*\).json/\1/')
    add_index_template $j
done

for i in config/user-roles/*.json
do
    j=$(echo $i | sed 's/.*\/\([^.]*\).json/\1/')
    add_user_role $j
done


for i in config/users/*.json
do
    j=$(echo $i | sed 's/.*\/\([^.]*\).json/\1/')
    add_user $j
done
